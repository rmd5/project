<?php

function getLoginForm() {
    $html = <<<html

    <div class="tab-pane fade in show active" role="tabpanel">
        <!--Body-->
        <div class="modal-body mb-1 panel2">
            <div class="no-account">
                Don't have an account? <a onclick="changeNavLogin('reg', 'login', 'que', 'panel2', 'panel1', 'panel3');"><strong>Sign up here <i class="fas fa-arrow-right"></i></strong></a>
            </div>

            <br>

            <!-- Email -->
            <div class="md-form">
                <label for="materialLoginFormEmail">E-mail</label><br>
                <input type="email"
                    id="materialLoginFormEmail"
                    class="form-control form-control-sm validate"
                    name="email"
                    required size="20"
                    placeholder="Enter your email"
                    value=""/>
            </div>

            <br>

            <!-- Password -->
            <div class="md-form">
                <label for="materialLoginFormPassword">Password</label><br>
                <input type="password" id="materialLoginFormPassword" placeholder="Enter your password" class="form-control" name="password"/>
            </div>

            <br>

            <div id="loginErrorBox" class="text-center alert alert-danger">
                <strong id="loginErrorDisplay"></strong>
            </div>

            <!-- Sign in button -->
            
            <button id="login_button" onclick="sendLoginRequest()" class="btn">Log in</button>
        </div>
    </div>
    <!--Panel 1 login Tab -->
html;

return $html;
}

?>