<?php

function getRegisterForm() {
    $html = <<<html

    <!--Panel 2 register-->
    <div class="tab-pane fade panel2" role="tabpanel">

        <!--Body-->
        <!--Card content-->
        <div class="card-body px-lg-5 pt-0">

            <!-- Form -->
            <!-- Email -->
            <div class="md-form">
                <label for="materialRegisterFormEmail">E-mail</label><br>
                <input type="email"
                    id="materialRegisterFormEmail"
                    class="form-control form-control-sm validate hold-back"
                    name="email"
                    required size="20"
                    placeholder="Enter your e-mail address"
                    value=""/>
            </div>

            <br>

            <div class="form-row">
                <div class="col-sm form-sm mb-5">
                    <!-- First name -->
                    <div class="md-form">
                        <label for="materialRegisterFormFirstName">First name</label><br>
                        <input type="text"
                            id="materialRegisterFormFirstName"
                            class="form-control form-control-sm validate"
                            name="first_name"
                            required size="20"
                            placeholder="Enter your first name"
                            value=""/>
                    </div>
                </div>

                <br>

                <div class="col-sm form-sm mb-5">
                    <!-- Last name -->
                    <div class="md-form drop-off">
                        <label for="materialRegisterFormLastName">Last name</label><br>
                        <input type="text"
                            id="materialRegisterFormLastName"
                            class="form-control form-control-sm validate"
                            name="last_name"
                            required size="20"
                            placeholder="Enter your last name"
                            value=""/>
                    </div>
                </div>
            </div>

            <br>

            <!-- Password -->
            <div class="md-form">
                <label data-error="wrong" data-success="right"
                    for="materialRegisterFormPassword1">Password</label><br>
                <input aria-describedby="materialRegisterFormPasswordHelpBlock"
                    class="form-control form-control-sm validate drop-up"
                    id="materialRegisterFormPassword1"
                    name="pass1"
                    required
                    placeholder="Enter your password"
                    size="20" type="password"/>
                    <br>
                <small id="materialRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
                    At least 8 characters and 1 digit
                </small>
            </div>

            <br>

            <div class="md-form">
                <label data-error="wrong" data-success="right" for="materialRegisterFormPassword2">Confirm password</label><br>
                <input type="password"
                    id="materialRegisterFormPassword2"
                    class="form-control form-control-sm validate drop-off"
                    aria-describedby="materialRegisterFormPasswordHelpBlock"
                    name="pass2"
                    placeholder="Confirm your password"
                    required size="20"/>
            </div>

            <div id="registerErrorBox" class="text-center alert alert-danger">
                <strong id="registerErrorDisplay"></strong>
            </div>

            <br>

            <div class="container">
                <div class="row">
                    <div class="col">
                        <small>By signing up you agree to our <a href="" target="_blank"><strong>terms of service</strong></a> </small> <!-- Terms of service -->
                    </div>
                </div>
            </div>

            <br>

            <!-- Sign up button -->
            <div>
                <button id="register-button" onclick="sendRegisterRequest()" class="btn" type="submit" value="submit">Sign up</button>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="registrationSuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Welcome!</h5>
                            <button type="button" onclick="location.reload()" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <div class="text-center">
                                <img class="confirmation-logo" src="img/favicon.png">
                                <p class="text-center"><p id="modalUserName"></p><br>Thank you for registering for bubble!<br/>Please return to the <a href="index.php">Login Page</a> to login</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" onclick="location.reload()" class="btn btn-secondary" data-dismiss="modal">Login</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
html;

return $html;
}

?>