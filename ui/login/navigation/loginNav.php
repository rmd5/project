<?php

function getLoginNav() {
    $html = <<<html
    <div class="footer">
        <div class="footer-loc footer-active" id="login" onclick="changeNavLogin('login', 'reg', 'que', 'panel1', 'panel2', 'panel3');">
            <div class="fas fa-user" style="margin-top:2px"></div>
        </div>
        <div class="footer-loc footer-lines">
            <div class="footer-line"></div>
        </div>
        <div class="footer-loc" id="reg" onclick="changeNavLogin('reg', 'login', 'que', 'panel2', 'panel1', 'panel3');">
            <div class="fas fa-user-plus" style="margin-top:2px;margin-left:2px;"></div>
        </div>
        <div class="footer-loc footer-lines">
            <div class="footer-line"></div>
        </div>
        <div class="footer-loc" id="que" onclick="changeNavLogin('que', 'reg', 'login', 'panel3', 'panel2', 'panel1');">
            <div class="fas fa-question" style="margin-top:4px;margin-left:-2px;"></div>
        </div>
    </div>
html;

return $html;
}

?>