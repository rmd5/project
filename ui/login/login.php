<?php

function generateLogin(){
    include "./ui/login/forms/loginForm.php";
    include "./ui/login/forms/registerForm.php";
    include "./ui/login/navigation/loginNav.php";
    $loginForm = getLoginForm();
    $registerForm = getRegisterForm();
    $navigation = getLoginNav();

    $html = <<<htmlPage
    <!--Modal cascading tabs-->
    <div class="login-page page">
        <table style="width:100%;" >
            <tr>
                <td class="align-middle">
                    <div class="text-center">
                        <img class="front-img" src="img/favicon.png">
                        <h2 class="app-name">Recharge.</h2>
                        <h4 class="app-slogan">Recharge your batteries.</h4>
                    </div>

                    $navigation

                    <!-- Tab panels -->
                    <div class="tab-content login-tabs">
                        <!--Panel 1 login Tab panel -->
                        <table style="height:80%; color:#f0ead6; table-layout:fixed;">
                            <tbody class="login-tbody">
                                <tr valign="top">
                                    <td id="panel1" class="login-tab">
                                        $loginForm
                                    </td>
                                    <td id="panel2" class="login-tab">
                                        $registerForm
                                    </td>
                                    <td id="panel3" class="login-tab">
                                        <!--Panel 3 Forgot Password Tab panel -->
                                        <div class="tab-pane fade panel2" role="tabpanel">

                                            <!--Body-->
                                            <div class="modal-body mb-1 about-section" style="text-align:justify;text-align-last:center;">
                                                <div><h4 style="font-weight:500!important"><strong>About Recharge</strong></div><br>
                                                Whether you need to ground yourself in your surroundings, focus on your breathing to allow
                                                for meditation and reducing your heart rate, or to listen to some calming natural sounds, Recharge
                                                is the app for you.
                                                <br><br>
                                                Brought to you by <a href="https://rorydobson.com" target="_blank"><strong>Rory Dobson<strong></a>
                                            </div>
                                        </div>
                                        <!--Panel 3 Forgot Password Tab -->
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
htmlPage;
    return $html;
}
?>