<?php

function getLoader() {
    $html = <<<html
        <div class="middle-container" id="loading">
            <div class="top loading" data-color="1">
                <div class="section-content">
                    <div class="section-info">
                        <div id="loader" class="loader"></div>
                    </div>
                </div>
            </div>
        </div>
html;

    return $html;
}

?>