<?php
//GENERATE MAIN TABS, AND ADD CONTENT INTO TABS

function generateCalm(){

    include "./ui/mainNav.php";
    $navigation = getMainNav();

    include "./ui/loader.php";
    $loader = getLoader();

    $home = "index.php";

    $array = array("Blue","Wooden","Tall","Red","Metallic","Round","Green","Plastic","Square","Yellow","Pink","Purple","Orange","White","Black","Soft","Hard","Bright","Small","Large");
    $rand_var = '';
    $find_num = rand(0,(count($array) - 1));
    $rand_var = $array[$find_num];

    $html = <<<html

    <div class="page" id="wrapper">
        <table class="main-table" id="nav-table">
            <tr valign="top">
                <td class="main-page" id="nav-page">
                    <div id="main">
                        <div class="section-title">
                            <h2><strong><a class="back-arrow" onclick="navTo('$home')"><i class="fas fa-arrow-left"></i></a> Calming</strong><strong class="burger" onclick="nav_display()"><i class="fas fa-bars"></i></strong></h2>
                        </div>

                        <div class="footer page-nav">
                            <div class="footer-loc footer-active" id="heart" onclick="changeNav('heart', 'dna', 'brain');">
                                <div class="fas fa-heartbeat" style="margin-top:5px"></div>
                            </div>
                            <div class="footer-loc footer-lines">
                                <div class="footer-line"></div>
                            </div>
                            <div class="footer-loc" id="dna" onclick="changeNav('dna', 'heart', 'brain');">
                                <div class="fas fa-dna" style="margin-top:2px"></div>
                            </div>
                            <div class="footer-loc footer-lines">
                                <div class="footer-line"></div>
                            </div>
                            <div class="footer-loc" id="brain" onclick="changeNav('brain', 'dna', 'heart');">
                                <div class="fas fa-brain" style="margin-top:2px"></div>
                            </div>
                        </div>

                        <div class="top calming-exercise">
                            <div class="middle-wrap">
                                <div class="middle-container">
                                    <div class="middle top-border">
                                        <div class="section-content">
                                            <div class="section-info">
                                                <h3>Name three items in the room that are:<br><strong id="item-wrap"><strong id="item" style="margin-top:10px">$rand_var</strong></strong></h3>
                                                <h3 onclick="changeItem();" style="color:#0677a1;cursor:pointer;">Next</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>

                $loader
                
                $navigation

            </tr>
        </table>
    </div>
html;

    return $html;
}
