<?php

function getDeleteModal() {
    $html = <<<html
    <div class="modal" id="delete-account">
        <div class="modal-content">
            <h3 class="modalTitle">
                <strong>Are you sure?</strong>
            </h3>
            <span class="close" onclick="closeModal('delete-account')"><i class="fas fa-times"></i></span>
            <h4 class="modal-text">
                This will permanently delete your account and can't be undone.
                <div class="delete-btns">
                    <button onclick="deleteAccount()" class="btn">Yes</button>
                    <button onclick="closeModal('delete-account')" class="btn">No</button>   
                </div>
            </h4>
        </div>
    </div>
html;

return $html;
}