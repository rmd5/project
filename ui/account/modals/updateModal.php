<?php

include_once './required/config.php';

function getUpdateModal() {
    global $db;

    session_start();
    $first_name = $_SESSION['first_name'];
    $user_id = $_SESSION['user_id'];
    session_write_close();

    $stmt = $db->prepare("SELECT * FROM users WHERE id = ?");
    $stmt->bind_param("i", $user_id);
    $stmt->execute();

    $result = $stmt->get_result();
    if ($result->num_rows === 1) {
        extract($result->fetch_assoc());
    
        $html = <<<html
            <div class="modal" id="update-account">
                <div class="modal-content">
                    <h3 class="modalTitle">
                        <strong>Update Account</strong>
                    </h3>
                    <span class="close" onclick="closeModal('update-account')"><i class="fas fa-times"></i></span>
                    <h4 class="modal-text">
                        <div class="md-form">
                            <label for="updateEmail">E-mail</label><br>
                            <input type="email"
                                id="updateEmail"
                                class="form-control form-control-sm validate hold-back"
                                name="email"
                                required size="20"
                                placeholder="$email"
                                value=""/>
                        </div>

                        <br>

                        <div class="form-row">
                            <div class="col-sm form-sm mb-5">
                                <!-- First name -->
                                <div class="md-form">
                                    <label for="updateFirstName">First name</label><br>
                                    <input type="text"
                                        id="updateFirstName"
                                        class="form-control form-control-sm validate"
                                        name="first_name"
                                        required size="20"
                                        placeholder="$first_name"
                                        value=""/>
                                </div>
                            </div>

                            <br>

                            <div class="col-sm form-sm mb-5">
                                <!-- Last name -->
                                <div class="md-form drop-off">
                                    <label for="updateLastName">Last name</label><br>
                                    <input type="text"
                                        id="updateLastName"
                                        class="form-control form-control-sm validate"
                                        name="last_name"
                                        required size="20"
                                        placeholder="$last_name"
                                        value=""/>
                                </div>
                            </div>

                            <br>

                            <div>
                                <button id="update-button" onclick="updateAccount('updateFirstName', 'updateLastName', 'updateEmail')" class="btn" type="submit" value="submit">Update</button>   
                            </div>
                        </div>
                    </h4>
                </div>
            </div>
html;
    }

return $html;
}