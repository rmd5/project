<?php
//GENERATE MAIN TABS, AND ADD CONTENT INTO TABS
include_once './required/config.php';

function generateAccount(){
    global $db;

    session_start();
    $first_name = $_SESSION['first_name'];
    $user_id = $_SESSION['user_id'];
    session_write_close();

    $stmt = $db->prepare("SELECT * FROM users WHERE id = ?");
    $stmt->bind_param("i", $user_id);
    $stmt->execute();

    $result = $stmt->get_result();
    if ($result->num_rows === 1) {
        extract($result->fetch_assoc());

        include "./ui/mainNav.php";
        $navigation = getMainNav();

        include "./ui/account/modals/deleteModal.php";
        $deleteModal = getDeleteModal();

        include "./ui/account/modals/updateModal.php";
        $updateModal = getUpdateModal();

        include "./ui/loader.php";
        $loader = getLoader();

        $home = "index.php";

        $html = <<<html

        <div class="page" id="wrapper">

            <table class="main-table" id="nav-table">
                <tr valign="top">
                    <td class="main-page" id="nav-page">

                        <div class="section-title">
                            <h2><strong><a class="back-arrow" onclick="navTo('$home');"><i class="fas fa-arrow-left"></i></a> Account</strong><strong class="burger" onclick="nav_display()"><i class="fas fa-bars"></i></strong></h2>
                        </div>

                        <div class="top">
                            <div class="middle-wrap">
                                <div class="middle-container" id="main">
                                    <div class="middle top-border">
                                        <div class="section-content">
                                            <div class="section-info">
                                                <table class="account-table">
                                                    <tr>
                                                        <td class="account-heading">
                                                            <strong>Name:</strong>
                                                        </td>
                                                        <td class="account-detail">
                                                            $first_name $last_name
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="account-heading">
                                                            <strong>Email:</strong>
                                                        </td>
                                                        <td class="account-detail">
                                                            $email
                                                        </td>
                                                    </tr>
                                                </table>
                                                <button class="btn update-btn" onclick="openModal('update-account')">Update Account</button>
                                                <button class="btn update-btn" onclick="openModal('delete-account')">Delete Account</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </td>

                    $loader

                    $updateModal

                    $deleteModal

                    $navigation
                    
                </tr>
            </table>
        </div>
html;
    }

    return $html;
}