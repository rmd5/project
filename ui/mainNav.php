<?php

function getMainNav() {
    $home = "index.php";
    $account = "index.php?action=account";
    $help = "index.php?action=help";

    $html = <<<html
    <div>
        <h3 class="close-x" onclick="nav_close()">
            <i class="fas fa-times"></i>
        </h3>
    </div>
    <td class="menu-nav" id="nav-menu">
        <div>
            <h3>
                <a onclick="menu_nav('$home');">Home</a>
            </h3>
        </div>
        <div>
            <h3>
                <a onclick="menu_nav('$account');">Account</a>
            </h3>
        </div>
        <div>
            <h3>
                <a onclick="menu_nav('$help');">Help</a>
            </h3>
        </div>
        <div>
            <h3>
                <a href="index.php?action=logout">Logout</a>
            </h3>
        </div>
    </td>
html;

return $html;
}

?>