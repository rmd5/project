<?php

function getButterfly() {
    $calm = "index.php?action=calm";

    $html = <<<html
    <div class="middle-container" id="main">
        <div class="top flower" id="butterfly" data-color="1">
            <div class="section-content">
                <div class="section-info">
                    <div class="flower-wrap flower-wrap-left">
                        <a onclick="navTo('$calm')">
                            <div id="flower-top-left" class="flower-display flower-display-left">
                                Calming Exercises
                            </div>
                        </a>
                    </div>

                    <div class="flower-wrap flower-wrap-right">
                        <div id="flower-top-right" class="flower-display flower-display-right">
                            Guided Breathing Exercises
                        </div>
                    </div>

                    <div class="flower-wrap flower-wrap-left flower-wrap-bottom">
                        <div id="flower-bottom-left" class="flower-display flower-display-left">
                            Focus Music
                        </div>
                    </div>

                    <div class="flower-wrap flower-wrap-right flower-wrap-bottom">
                        <div id="flower-bottom-right" class="flower-display flower-display-right">
                            My Stats
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
html;

return $html;
}