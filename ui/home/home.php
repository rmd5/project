<?php
//GENERATE MAIN TABS, AND ADD CONTENT INTO TABS
function generateHome(){
    include "./ui/mainNav.php";
    include "./ui/home/butterfly.php";
    $navigation = getMainNav();
    $butterfly = getButterfly();

    include "./ui/loader.php";
    $loader = getLoader();

    $first_name = $_SESSION['first_name'];

    $html = <<<html

    <div class="page" id="wrapper home-page">

        <table class="main-table" id="nav-table">
            <tr valign="top">
                <td class="main-page" id="nav-page">

                    <div class="section-title">
                        <h2><strong>Home</strong><strong class="burger" onclick="nav_display()"><i class="fas fa-bars"></i></strong></h2>
                    </div>
                    
                    $butterfly

                </td>

                $loader
                
                $navigation

            </tr>
        </table>
    </div>
html;

    return $html;
}