<?php
include_once 'config.php';

//BEGIN SESSION
session_start();
$user_id = $_SESSION['user_id'];

//END SESSION
session_write_close();

//GRAB INFORMATION SENT OVER AJAX


$type = filter_input(INPUT_POST, "type", FILTER_SANITIZE_STRING);
if($type == FALSE){
    echo("{\"error\":\"Invalid type\"}");
    exit(0);
}

if($type == "account"){
    
    //GET FIRST NAME
    $first_name = filter_input(INPUT_POST, "fn", FILTER_SANITIZE_STRING);
    if($first_name == FALSE && $_POST['fn'] != ""){
        echo("{\"error\":\"Invalid first name\"}");
        exit(0);
    }

    //GET VALID LAST NAME
    $last_name = filter_input(INPUT_POST, "ln", FILTER_SANITIZE_STRING);
    if($last_name == FALSE && $_POST['ln'] != ""){
        echo("{\"error\":\"Invalid last name\"}");
        exit(0);
    }

    //GET VALID EMAIL
    $email = filter_input(INPUT_POST, "email", FILTER_VALIDATE_EMAIL);
    if($email == FALSE && $_POST['email'] != ""){
        echo("{\"error\":\"Invalid email address\"}");
        exit(0);
    }

        
    //If an edit has been made, update the user information with the new value
    if($first_name != ''){
        $stmt = $db->prepare("UPDATE users SET first_name = ? WHERE id = ?");
        $stmt->bind_param("si", $first_name, $user_id);
        $stmt->execute();
        $stmt->close();
    }

    //If an edit has been made, update the user information with the new value
    if($last_name != ''){
        $stmt = $db->prepare("UPDATE users SET last_name = ? WHERE id = ?");
        $stmt->bind_param("si", $last_name, $user_id);
        $stmt->execute();
        $stmt->close();
    }

    //If an edit has been made, update the user information with the new value
    if($email != ''){
        $stmt = $db->prepare("UPDATE users SET email = ? WHERE id = ?");
        $stmt->bind_param("si", $email, $user_id);
        $stmt->execute();
        $stmt->close();
    }

    echo("{\"success\":\"Success \"}");

} else {
    echo("{\"error\":\"Invalid request \"}");
}



?>