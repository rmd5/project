<?php
include_once 'config.php';

//BEGIN SESSION
session_start();
$user_id = $_SESSION['user_id'];

//GRAB INFORMATION SENT OVER AJAX

$stmt = $db->prepare("DELETE FROM users WHERE id = ?");
$stmt->bind_param("i", $user_id);
$stmt->execute();
$stmt->close();

session_destroy();

echo("{\"success\":\"Success \"}");

session_write_close();

?>