// Animate butterfly navigation ---------------------------------------------------------------------------------------
window.addEventListener("load", fadeInButterfly);

function fadeInButterfly() {
    var length = 1000;
    var timer = 300;

    $("#flower-top-left").css('visibility','hidden');
    $("#flower-top-right").css('visibility','hidden');
    $("#flower-bottom-right").css('visibility','hidden');
    $("#flower-bottom-left").css('visibility','hidden');

    $("#flower-top-left").css('visibility','visible').hide().fadeIn(length);
    setTimeout(function() {
        $("#flower-top-right").css('visibility','visible').hide().fadeIn(length);
        setTimeout(function() {
            $("#flower-bottom-right").css('visibility','visible').hide().fadeIn(length);
            setTimeout(function() {
                $("#flower-bottom-left").css('visibility','visible').hide().fadeIn(length);
                setTimeout(function() {
                    $("#flower-top-left").css('visibility','visible');
                    $("#flower-top-right").css('visibility','visible');
                    $("#flower-bottom-right").css('visibility','visible');
                    $("#flower-bottom-left").css('visibility','visible');
                }, timer);
            }, timer);
        }, timer);
    }, timer);
}

// \\ Animate butterfly navigation ---------------------------------------------------------------------------------------