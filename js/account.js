// Alter account functions -------------------------------------------------------------------------------------------

// Update account
function updateAccount(fn, ln, email) {
    let url = "required/action_updateAccount.php";
    var first_name = $('#' + fn).val();
    var last_name = $('#' + ln).val();
    var email_adr = $('#' + email).val();
    
    //Call action device to update account
    $.ajax({
        type:'POST',
        url: url,
        data:{type: "account", fn: first_name, ln: last_name, email: email_adr},
        success:function(data){
            //PARSE RESPONSE JSON DATA
            var result = JSON.parse(data);

            //ADD ROOM ERROR, DISPLAY ERROR TO USER
            if(result.error){
                alert(result.error);
            }
            if(result.success){
                navTo('index.php?action=account');
            }
        },
        error: function(data){
            alert("error!");
        }
    });
}

// Delete account
function deleteAccount() {
    let url = "required/action_deleteAccount.php";
    
    //Call action device to update account
    $.ajax({
        type:'POST',
        url: url,
        success:function(data){
            //PARSE RESPONSE JSON DATA
            var result = JSON.parse(data);

            //ADD ROOM ERROR, DISPLAY ERROR TO USER
            if(result.error){
                alert(result.error);
            }
            if(result.success){
                location.reload();
            }
        },
        error: function(data){
            alert("error!");
        }
    });
}

// \\ Alter account functions -------------------------------------------------------------------------------------------
