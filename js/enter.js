// Press button on enter key hit -------------------------------------------------------------------------------------

//Enter login details
var panel1 = document.getElementById("panel1");
if(panel1){
    panel1.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            document.getElementById("login_button").click();
        }
    });
}

//Enter registration details
var panel2 = document.getElementById("panel2");
if(panel2){
    panel2.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            document.getElementById("register-button").click();
        }
    });
}

//Enter update account details
var account = document.getElementById("update-account");
if(account){
    account.addEventListener("keyup", function(event) {
        if (event.keyCode === 13) {
            document.getElementById("update-button").click();
        }
    });
}

// \\ Press button on enter key hit -----------------------------------------------------------------------------------