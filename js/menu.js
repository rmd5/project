// Open and close nav menu ------------------------------------------------------------------------------------------------------
function nav_display() {
    var menu = document.getElementById("nav-menu");
    menu.scrollIntoView();
}

function nav_close() {
    var page = document.getElementById("main-body");
    page.scrollIntoView();
}

function navTo(file) {
    document.getElementById('loader').style.display = "block";
    document.getElementById('main').style.display = "none";
    document.getElementById('nav-page').style.visibility = "hidden";
    this.document.location.href = file;
    // $("#entire-page").load(file);
    // setTimeout(function() {
    //     nav_close();
    //     if(file == "index.php"){
    //         setTimeout(function(){
    //             fadeInButterfly();
    //         }, 300);
    //     }
    // }, 500);
}

function menu_nav(file) {
    nav_close();
    document.getElementById('loader').style.display = "block";
    document.getElementById('main').style.display = "none";
    document.getElementById('nav-page').style.visibility = "hidden";
    setTimeout(function() {
        navTo(file);
    }, 500);
}

// \\ Open and close nav menu ------------------------------------------------------------------------------------------------------