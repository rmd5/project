// Slide navgation ------------------------------------------------------------------------------------------------

function changeNav(newNav, old1, old2) {
    var newNav = document.getElementById(newNav);
    var old1 = document.getElementById(old1);
    var old2 = document.getElementById(old2);

    newNav.classList.add('footer-active');
    old1.classList.remove('footer-active');
    old2.classList.remove('footer-active');
}

// Login slide
function changeNavLogin(newNav, old1, old2, newPanel, oldPanel1, oldPanel2) {
    var newNav = document.getElementById(newNav);
    var old1 = document.getElementById(old1);
    var old2 = document.getElementById(old2);
    var newPanel = document.getElementById(newPanel);
    var oldPanel1 = document.getElementById(oldPanel1);
    var oldPanel2 = document.getElementById(oldPanel2);

    newNav.classList.add('footer-active');
    old1.classList.remove('footer-active');
    old2.classList.remove('footer-active');
    newPanel.style.display = "table-cell";
    newPanel.scrollIntoView();

}

// \\ Slide navgation ------------------------------------------------------------------------------------------------