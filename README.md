# Summer Project

### Project worked on through summer 2020. The goal is to create the framework of an app, without using any libraries such as MD bootstrap, that can be altered to fit any future apps.

### Current working features:
- Login Page
- Registration
- Butterfly style home page navigation
- Slide out menu
- Loading icon before page load
- Account page displaying database details
- Update account function
- Delete account function
- Massively re-formatted to make reuse of items easier
- Salt and Pepper password security
- Sanitized inputs to revent XSS
- Prepared statements to prevent SQL injections

<!-- https://www.markdownguide.org/cheat-sheet/ -->