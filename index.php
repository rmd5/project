<?php
    //////////////////////////////// BUBBLE APP WEB GENERATOR /////////////////////////////////////
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    //SETUP REQUIREMENTS
    require './required/config.php';


    //REQUIRES SESSIONS
    session_start();
    //SETUP VARIABLES
    $isLoggedIn = isset($_SESSION['user_id']);
    //CLOSE SESSION
    session_write_close();

    //GRAB FILE CONTENTS FROM PREGENERATED HTML TEMPLATE
    $pageHTML = file_get_contents('html/mainTemplate.html');

    $content = '';

    //GENERATE REMAINING PAGE
    if($isLoggedIn){
        $content = generate_loggedInContent();
    }else{
        $content = generate_loginPage();
    }

    //REPLACE TAGS INSIDE TEMPLATE WITH GENERATED CONTENT
    $pageHTML = str_replace('$MAIN_BODY', $content, $pageHTML);

    //ECHO HTML TO PAGE
    echo $pageHTML;
    exit();

    //////////////////////////////////////////////////////////////////// FUNCTIONS ///////////////////////////////////////////////////////////////////////

    /**
     * GENERATOR FUNCTION FOR NON-LOGGED IN USERS, E.G. LOGIN, SIGNUP, FORGOT PASSWORD
     */
    function generate_loginPage(){
        global $templatesDir;
        //GET URL ACTION
        $action = '';
        if(isset($_GET['action'])) $action = $_GET['action'];
        
        $html = '';

        //SWITCH DEPENDING ON URL ACTION
        /**
         * SWITCH BASED ON ACTION.
         * INCLUDE REQUIRED FILE, THEN CALL FUNCTION DEFINED INSIDE PHP FILE
         * THIS ALLOWS US TO GENERATE THE CONTENT THROUGH PHP, THEN INSERT THAT CONTENT INTO THE PREGENERATED HTML-TEMPLATE
         */       
        switch($action){
            default:
                include "./ui/login/login.php";
                $html .= generateLogin();
            break;
        }

        return $html;
    }

    /**
     * GENERATOR FUNCTION FOR GENERATING LOGGED IN CONTENT
     */
    function generate_loggedInContent(){
        //GET URL ACTION
        $action = '';
        if(isset($_GET['action'])) $action = $_GET['action'];
        
        $html = '';  

            //SWITCH DEPENDING ON URL ACTION
            /**
             * SWITCH BASED ON ACTION.
             * INCLUDE REQUIRED FILE, THEN CALL FUNCTION DEFINED INSIDE PHP FILE
             * THIS ALLOWS US TO GENERATE THE CONTENT THROUGH PHP, THEN INSERT THAT CONTENT INTO THE PREGENERATED HTML-TEMPLATE
             */
            switch($action){
                case 'calm':
                    include './ui/calm.php';
                    $html .= generateCalm();
                    break;
                case 'logout':
                    include './required/action_logout.php';
                    $html .= generateLogout();
                    break;
                case 'account':
                    include './ui/account/account.php';
                    $html .= generateAccount();
                    break;
                // case 'help':
                //     include './ui/help.php';
                //     $html .= generateHelp();
                //     break;
                // case 'breath':
                //     include './ui/breath.php';
                //     $html .= generateBreath();
                //     break;
                // case 'music':
                //     include './ui/music.php';
                //     $html .= generateMusic();
                //     break;
                // case 'stats':
                //     include './ui/stats.php';
                //     $html .= generateStats();
                //     break;
                default:
                    include "./ui/home/home.php";
                    $html .= generateHome();
                    break;
            }
        return $html;
    }
?>


